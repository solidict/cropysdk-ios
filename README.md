SDK Install

MobileCoreServices.framework
SystemConfiguration.framework
MessageUI.framework
Social.framework
Photos.framework
AssetsLibrary.framework
CoreTelephony.framework
imglyKit.framework  (You can find this framework in example project)
You should drag and drop "Source" and "cropy_images" files into your project. 

*** Add "imglyKit.framework" by clicking + button on Target > General > "Embeded Binaries"
*** Copy and paste "$BUILT_PRODUCTS_DIR/$FRAMEWORKS_FOLDER_PATH/imglyKit.framework/strip-framework.sh" to Build Phase > Run Script


* Add the following source code to info.plist like source code

```groovy
<key>LSApplicationQueriesSchemes</key>
<array>
<string>linkedin</string>
<string>bip</string>
<string>whatsapp</string>
<string>fbapi</string>
<string>fbapi20130214</string>
<string>fbapi20130410</string>
<string>fbapi20130702</string>
<string>fbapi20131010</string>
<string>fbapi20131219</string>
<string>fbapi20140410</string>
<string>fbapi20140116</string>
<string>fbapi20150313</string>
<string>fbapi20150629</string>
<string>fbauth</string>
<string>fbauth2</string>
<string>fb-messenger-api20140430</string>
</array>
```

CRYConfigurasyon Class is used to set configurations below for SDK integration. 

CRYConfigurasyon parameters:

Text

-headerText

Background Colors

-headerColor

-tabToolColor

-topToolBarColor

-headerTitleColor

-activeTextColor

-passiveTextColor

-cropBorderColor

Modules

-shareType //SharedTypeURL, SharedTypeImage, SharedTypeDefault

-enableHeaderTitle

-enableCrop

-enableRotation

-enableAdjustment

-enableFilter

-enableText

-enableArrow

-enableLine

-enablePen

-enableRectangel

-enableCircle

-enableFrame

-enablePixel

-enableFocus

-enableCaps

-enableShare


Example:

[CRYConfiguration sharedInstance].headerText = @“Hello World!”;
[CRYConfiguration sharedInstance].headerColor = [UIColor redColor];
[CRYConfiguration sharedInstance].enablePixel = NO;
[CRYConfiguration sharedInstance].shareType = SharedTypeURL;

#import “CropySDK.h" when configuration settings completed.

Add <TOCropViewControllerDelegate, SharedTypeDelegate>

Then follow these steps;

```groovy
UIImage *image = [UIImage imageNamed:@"image.jpg"];
    
CRYCropNavigationController *cropController = [CRYCropNavigationController startEditControllerWithImage:image];
[cropController setCropDelegate:self];
cropController.sharedDelegate = self;
[self presentViewController:cropController animated:YES completion:nil];
```

Delegates

```groovy
-(void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    
    SecondViewController *second = [[SecondViewController alloc]init];
    second.comingImage = image;
    [self.navigationController pushViewController:second animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)getEditedImage:(UIImage *)sharedImage{



}

-(void)getSharedUrl:(NSString *)sharedUrl{

    NSLog(@"SharedURL = %@", sharedUrl);

}
```
