//
//  SecondViewController.m
//  TestCropySDK
//
//  Created by Alper Kırdök on 22/06/16.
//  Copyright © 2016 Alper Kırdök. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    cropedImageView.image = self.comingImage;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
