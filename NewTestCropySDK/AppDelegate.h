//
//  AppDelegate.h
//  NewTestCropySDK
//
//  Created by Alper KIRDÖK on 22/06/16.
//  Copyright © 2016 Alper KIRDÖK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

