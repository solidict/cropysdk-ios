//
//  UITextViewCustomCursor.h
//  CropyMain
//
//  Created by Selim Savsar on 14/11/16.
//  Copyright © 2016 Alper KIRDÖK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextViewCustomCursor : UITextView

@property (nonatomic, assign)BOOL cursorON;

@end
