//
//  CRYRotateImageDelegate.h
//  CropySDK
//
//  Created by Alper KIRDÖK on 19/07/16.
//  Copyright © 2016 SolidICT. All rights reserved.
//

#ifndef CRYRotateImageDelegate_h
#define CRYRotateImageDelegate_h

@protocol CRYRotateImageDelegate <NSObject>

-(void)getRotatedImage:(UIImage *)rotatedImage;

@end

#endif /* CRYRotateImageDelegate_h */
