//
//  CropyConstants.h
//  CropyMain
//
//  Created by Ugur Eratalar on 30/06/16.
//  Copyright © 2016 Alper KIRDÖK. All rights reserved.
//

#ifndef CropyConstants_h
#define CropyConstants_h

#define UNAUTHORIZED_COUNT @"Unauthorized"
#define X_REMEMBER_ME @"X-RememberMe"
#define X_AUTHTOKEN @"X-AuthToken"
#define USER_UUID @"UUID"
//#define WARNING @"Uyarı"
//#define INFO @"Bilgi"
#define LOGIN @"Giriş"
#define BOOKMARKS @"bookmarks"
#define TOTAL_IMAGE_COUNT 10

// test
//#define CROPY_BASE_URL @"https://fast-depths-2920.herokuapp.com"
//#define CROPY_API_KEY @"fa2726d3-caa6-425a-83c0-424eea53ce46"
//prod

#define CROPY_BASE_URL @"https://cropy.com/upload"
#define CROPY_API_KEY @"cbe1a677-7f95-4ad8-ab0f-5809470fc53a"

#define ENABLE_LOG YES

#endif /* CropyConstants_h */
