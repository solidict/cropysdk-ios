//
//  MainViewController.h
//  TestCropySDK
//
//  Created by Alper Kırdök on 22/06/16.
//  Copyright © 2016 Alper Kırdök. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UIGestureRecognizerDelegate>

- (IBAction)cropButtonTapped:(id)sender;
- (IBAction)rotateButtonTapped:(id)sender;

@end
