//
//  SecondViewController.h
//  TestCropySDK
//
//  Created by Alper Kırdök on 22/06/16.
//  Copyright © 2016 Alper Kırdök. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController{
    
    __weak IBOutlet UIImageView *cropedImageView;
    
}

@property (nonatomic, strong) UIImage *comingImage;

@end
