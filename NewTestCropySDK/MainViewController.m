//
//  MainViewController.m
//  TestCropySDK
//
//  Created by Alper Kırdök on 22/06/16.
//  Copyright © 2016 Alper Kırdök. All rights reserved.
//

#import "MainViewController.h"
#import "SecondViewController.h"
#import "CropySDK.h"
//#import "CRYCropNavigationController.h"
#import "CRYConfiguration.h"

@interface MainViewController ()<CRYRotateImageDelegate, TOCropViewControllerDelegate, SharedTypeDelegate>

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (IBAction)cropButtonTapped:(id)sender {
    
    //Configuration Exp
    [CRYConfiguration sharedInstance].enablePixel = NO;
    [CRYConfiguration sharedInstance].headerColor = [UIColor redColor];
    [CRYConfiguration sharedInstance].shareType = SharedTypeURL;
    //[CRYConfiguration sharedInstance].shareType = SharedTypeImage;

    UIImage *image = [UIImage imageNamed:@"image.jpg"];
    
    CRYCropNavigationController *cropController = [CRYCropNavigationController startEditControllerWithImage:image];
    [cropController setCropDelegate:self];
    [cropController setShareEnabled:YES];
    cropController.sharedDelegate = self;
    [self presentViewController:cropController animated:YES completion:nil];
    
}

-(void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    
    SecondViewController *second = [[SecondViewController alloc]init];
    second.comingImage = image;
    [self.navigationController pushViewController:second animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)getEditedImage:(UIImage *)sharedImage{
    
    
    
}

-(void)getSharedUrl:(NSString *)sharedUrl{
    
    NSLog(@"SharedURL = %@", sharedUrl);
    
}

- (IBAction)rotateButtonTapped:(id)sender {
    
    CRYRotateImageViewController *rotateImage = [CRYRotateImageViewController getInstance];
    rotateImage.rotateDelegate = self;
    rotateImage.rotateImage = [UIImage imageNamed:@"image.jpg"];
    
    [self presentViewController:rotateImage animated:YES completion:nil];
    
}

-(void)getRotatedImage:(UIImage *)rotatedImage{
    
    SecondViewController *second = [[SecondViewController alloc]init];
    second.comingImage = rotatedImage;
    [self.navigationController pushViewController:second animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
